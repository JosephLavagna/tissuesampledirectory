from flask import Flask, render_template, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from models import * #fixed circular import issue BUT bad pratice 
from flask import request, redirect

app = Flask(__name__)

#SQL DB Config
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///collectionSamples.db'

db = SQLAlchemy(app)


@app.route('/')
def index():
    col = Collection.query.order_by(Collection.id).all()
    for temp in col:
        print (temp.id)
    return render_template('index.html', col=col)


@app.route('/samples_page/<int:rel_id>')
def samples(rel_id):
    samp = SampleDetails.query.filter_by(collection_id=rel_id).all()
    for association in samp:
        print (association.id)
    return render_template('samples.html', samp=samp, collection_id=rel_id)

@app.route('/add/<int:col_id>', methods = ['POST', 'GET'])
def add(col_id):
    if request.method == 'POST':
        form_donor = request.form['donor']
        form_material = request.form['material']
        form_updated = request.form['updated']
        if form_donor and form_material and form_updated !="":
            new_sample = SampleDetails(collection_id=col_id, donor_count=form_donor, material_type=form_material, last_updated=form_updated)
        else:
            return redirect (url_for('samples', rel_id = col_id))
        

        try:
            db.session.add(new_sample)
            db.session.commit()
            return redirect (url_for('samples', rel_id = col_id))
        except:
            return 'Error: Sample not added'
    else:
        return render_template('samples.html')


if __name__ == "__main__": 
    app.run(debug=True)