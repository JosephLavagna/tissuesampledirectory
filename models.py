from app import db
from datetime import datetime

class Collection(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    disease_term = db.Column(db.String(128))
    title = db.Column(db.String(128))
    samples = db.relationship('SampleDetails', backref='collectionRel')

class SampleDetails(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    collection_id = db.Column(db.Integer, db.ForeignKey('collection.id'))
    donor_count = db.Column(db.Integer)
    material_type = db.Column(db.String(128))
    last_updated = db.Column(db.String(10))
    
    